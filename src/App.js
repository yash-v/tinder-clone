import './App.css'
import Header from './Components/Header';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import TinderCards from './Components/TinderCards'

function App() {
  return (
    <div className="App">
      <Router>

        <Header />

        <Route path='/' exact>
          <TinderCards />
        </Route>
        
        <Route path='/chat'>
          <h1>This is the Chat page.</h1>
        </Route>
        
      </Router>
    </div>
  );
}

export default App;
