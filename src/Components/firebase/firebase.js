import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBLCo27NMamE3gvpphZISt07HUMYF5IIUE",
  authDomain: "tinder-clone-d68ee.firebaseapp.com",
  projectId: "tinder-clone-d68ee",
  storageBucket: "tinder-clone-d68ee.appspot.com",
  messagingSenderId: "520149092975",
  appId: "1:520149092975:web:feeb135cefd0d1a23c9b9a"
};

const firebaseApp = firebase.initializeApp(firebaseConfig)
const database = firebaseApp.firestore()

export default database
